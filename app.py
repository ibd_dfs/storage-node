from apscheduler.schedulers.background import BackgroundScheduler
import os
import requests
import shutil
import threading

from flask import (
    Flask,
    request,
    jsonify,
    send_file,
)
STORAGE_PATH = os.environ.get('STORAGE_PATH', default='/var/storage')
app = Flask(__name__)

# Clear directory
shutil.rmtree(STORAGE_PATH, ignore_errors=True)

# Register into name node
requests.post(
    f'{os.environ["NAME_HOST"]}/node/register',
    data={
        'public_address': os.environ['PUBLIC_IP'],
        'private_address': os.environ['PRIVATE_IP'],
        'total_volume': os.environ['SERVER_CAPACITY']
    }
)

# healthcheck task


def healthcheck():
    requests.post(
        f'{os.environ["NAME_HOST"]}/node/healthcheck',
        data={
            'private_address': os.environ['PRIVATE_IP'],
        },
    )


# start the healthcheck task
scheduler = BackgroundScheduler()
job = scheduler.add_job(healthcheck, 'interval', minutes=1)
scheduler.start()

# kill the task on 500


@app.errorhandler(500)
def internal_error(error):
    job.remove()
    scheduler.shutdown()
    return "500 error"


@app.route('/file/create/', methods=['POST'])
def file_create():
    try:
        path = request.form['full_path']
    except Exception:
        path = request.json['full_path']
    full_path = os.path.join(STORAGE_PATH, path)
    if not os.path.exists(os.path.dirname(full_path)):
        os.makedirs(os.path.dirname(full_path))
    file = open(full_path, 'w')
    file.close()
    requests.post(
        f'{os.environ["NAME_HOST"]}/node/file_created',
        data={
            'private_address': os.environ['PRIVATE_IP'],
            'full_path': path,
            'replica': False,
        })
    return jsonify(
        isError=False,
        statusCode=201,
        data="File created successfully"
    ), 201


@app.route('/file/upload/', methods=['POST'])
def file_upload():
    try:
        path = request.form['full_path']
    except Exception:
        path = request.json['full_path']
    full_path = os.path.join(STORAGE_PATH, path)
    if not os.path.exists(os.path.dirname(full_path)):
        os.makedirs(os.path.dirname(full_path))
    file = request.files['file']
    file.save(full_path)
    requests.post(
        f'{os.environ["NAME_HOST"]}/node/file_created',
        data={
            'private_address': os.environ['PRIVATE_IP'],
            'full_path': path,
            'replica': False,
        })
    return jsonify(
        isError=False,
        statusCode=201,
        data="File created successfully"
    ), 201


@app.route('/file/download/', methods=['GET'])
def file_download():
    full_path = request.args['full_path']
    full_path = os.path.join(STORAGE_PATH, full_path)
    if os.path.isfile(full_path):
        return send_file(full_path, os.path.basename(full_path))
    else:
        return jsonify(
            isError=True,
            statusCode=404,
            data="File does not exist"
        ), 404


@app.route('/file/delete/', methods=['DELETE'])
def file_delete():
    try:
        path = request.form['full_path']
    except Exception:
        path = request.json['full_path']
    full_path = os.path.join(STORAGE_PATH, path)
    if os.path.isfile(full_path):
        os.remove(full_path)
        return jsonify(
            isError=False,
            statusCode=200,
            data="File removed successfully"
        ), 200
    else:
        return jsonify(
            isError=True,
            statusCode=404,
            data="File does not exist"
        ), 404


def copy(url, path_to, full_path_from, full_path_to):
    if not os.path.exists(os.path.dirname(full_path_to)):
        os.makedirs(os.path.dirname(full_path_to))
    resp = requests.get(f'{url}/file/download/?full_path={full_path_from}')
    with open(full_path_to, 'wb') as file:
        file.write(resp.content)
    requests.post(
        f'{os.environ["NAME_HOST"]}/node/file_created',
        data={
            'private_address': os.environ['PRIVATE_IP'],
            'full_path': path_to,
            'replica': full_path_from == path_to,
        }
    )


@app.route('/file/copy/', methods=['POST'])
def file_copy():
    try:
        url = request.form['ip_from']
        full_path_from = request.form['full_path_from']
        path_to = request.form['full_path_to']
    except Exception:
        url = request.json['ip_from']
        full_path_from = request.json['full_path_from']
        path_to = request.json['full_path_to']
    full_path_to = os.path.join(STORAGE_PATH, path_to)
    th = threading.Thread(target=copy, args=(
        url, path_to, full_path_from, full_path_to))
    th.start()
    return jsonify(
        isError=False,
        statusCode=200,
        data="File copying started"
    ), 200


@ app.route('/dir/delete/', methods=['DELETE'])
def directory_delete():
    try:
        path = request.form['full_path']
    except Exception:
        path = request.json['full_path']
    full_path = os.path.join(STORAGE_PATH, path)
    if os.path.isdir(full_path):
        shutil.rmtree(full_path)
        return jsonify(
            isError=False,
            statusCode=200,
            data="Directory removed successfully"
        ), 200
    else:
        return jsonify(
            isError=True,
            statusCode=404,
            data="Directory does not exist"
        ), 404


@ app.route('/file/move/', methods=['POST'])
def file_move():
    try:
        full_path_from = request.form['full_path_from']
        full_path_to = request.form['full_path_to']
    except Exception:
        full_path_from = request.json['full_path_from']
        full_path_to = request.json['full_path_to']

    full_path_from = os.path.join(STORAGE_PATH, full_path_from)
    full_path_to = os.path.join(STORAGE_PATH, full_path_to)
    if os.path.isfile(full_path_to):
        return jsonify(
            isError=True,
            statusCode=409,
            data="Destination file already exsists"
        ), 409

    if os.path.isfile(full_path_from):
        if not os.path.exists(os.path.dirname(full_path_to)):
            os.makedirs(os.path.dirname(full_path_to))
        os.rename(full_path_from, full_path_to)
        return jsonify(
            isError=False,
            statusCode=200,
            data="File moved successfully"
        ), 200
    else:
        return jsonify(
            isError=True,
            statusCode=404,
            data="File does not exist"
        ), 404


if __name__ == "__main__":
    app.run('0.0.0.0', port=5000)
