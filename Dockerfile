FROM python:3.8.6


WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
ENTRYPOINT [ "python3", "app.py"]