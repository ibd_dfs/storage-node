docker run -p 5000:5000 \
-e NAME_HOST=$1 \
-e PUBLIC_IP=http://$(curl http://169.254.169.254/latest/meta-data/public-ipv4):5000 \
-e PRIVATE_IP=http://$(curl http://169.254.169.254/latest/meta-data/local-ipv4):5000 \
-e SERVER_CAPACITY=$2 \
-e STORAGE_PATH="/var/storage" \
sgmakarov/storage